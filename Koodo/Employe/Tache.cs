﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koodo.Employe
{
    class Tache : Employe
    {
        #region Déclarations

        private decimal tauxHorraire;

        public decimal nbrHeures;

        #endregion Déclarations

        #region Constructeur
        public Tache(string nom, string telephone, string matricule, decimal tauxHorraire) : base(nom, telephone, matricule)
        {
            this.tauxHorraire = tauxHorraire;
        }

        #endregion Constructeur

        #region Getters

        public decimal TauxHoraire
        {
            get { return tauxHorraire; }
        }

        #endregion Getters

        #region Méthodes publiques

        public override decimal Salaire(bool reset = false)
        {
            decimal returnValue = tauxHorraire * nbrHeures;
            
            if (reset)
                nbrHeures = 0;
            return returnValue;
        }

        #endregion Méthodes publiques

    }
}
