﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koodo.Employe
{
    public class Bureau : Employe
    {
        #region Déclarations

        private decimal salaireFixe;

        #endregion Déclarations

        #region Constructeur

        public Bureau(string nom, string telephone, string matricule) : base(nom, telephone, matricule)
        {
            
        }

        public Bureau(string nom, string telephone, string matricule, decimal salaireFixe) : base(nom, telephone, matricule)
        {
            this.salaireFixe = salaireFixe;
        }

        #endregion Constructeur

        #region Getters

        public decimal SalaireFixe
        {
            get { return salaireFixe; }
        }

        #endregion Getters

        #region Méthodes publiques

        public override decimal Salaire(bool reset = false)
        {
            return salaireFixe;
        }

        #endregion Méthodes publiques

    }
}
