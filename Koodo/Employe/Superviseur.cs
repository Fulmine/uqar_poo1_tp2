﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koodo.Employe
{
    public class Superviseur : Bureau
    {

        #region Déclarations

        public decimal objectif;

        #endregion Déclarations

        #region Constructeur

        public Superviseur(string nom, string telephone, string matricule, decimal salaireFixe) : base(nom, telephone, matricule, salaireFixe)
        {

        }

        #endregion Constructeur

        #region Méthodes publiques

        /// Explications :
        /// Nous sommes parti du principe que le montant total des ventes des superviseurs 
        /// était en fonction du montant total des ventes de tous les commerciaux
        public override decimal Salaire(bool reset = false)
        {
            decimal totalVentes = 0;
            decimal returnValue = this.SalaireFixe;

            foreach (Employe employe in Koodo.employeList)
            {
                if (employe.GetType() == typeof(Commercial))
                {
                    totalVentes += ((Commercial)employe).Salaire(false);
                }
            }

            if (totalVentes >= objectif)
            {
                returnValue *= Convert.ToDecimal(1.10);
            }

            if (reset)
                objectif = 0;
            
            return returnValue;
        }

        #endregion Méthodes publiques

    }
}
