﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koodo.Employe
{
    public abstract class Employe
    {
        #region Déclarations

        protected string nom;

        protected string telelphone;

        protected string matricule;
        #endregion Déclaration

        #region Constructeur

        protected Employe(string nom, string telephone, string matricule)
        {
            this.nom = nom;
            this.telelphone = telephone;
            this.matricule = matricule;
        }

        #endregion Constructeur

        #region Getters 

        public String Nom
        {
            get { return this.nom; }
        }
        public String Telephone
        {
            get { return this.telelphone; }
        }

        public String Matricule
        {
            get { return this.matricule; }
        }

        #endregion Getters

        #region Méthodes publiques

        public abstract decimal Salaire(bool reset = false);

        public override string ToString()
        {
            return String.Format("Nom: {0} - Téléphone: {1} - Matricule: {2}",
                this.nom, 
                this.telelphone,
                this.matricule);
        }

        #endregion Méthodes publiques

    }
}
