﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koodo.Employe
{
    public class Commercial : Bureau
    {

        #region Déclarations

        public decimal montantVente;

        #endregion Déclarations

        #region Constructeur

        public Commercial(string nom, string telephone, string matricule, decimal salaireFixe) : base(nom, telephone, matricule, salaireFixe)
        {

        }

        #endregion Constructeur

        #region Méthodes publiques

        public override decimal Salaire(bool reset = false)
        {
            decimal returnValue = SalaireFixe + montantVente * 0.001m ;

            if (reset)
                montantVente = 0;
            return returnValue;
        }

        #endregion Méthodes publiques

    }
}
