﻿namespace Koodo
{
    partial class mainForm
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Employés « Bureau »", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("Employés « Tâches »", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("Employés « Commerciaux »", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup4 = new System.Windows.Forms.ListViewGroup("Employés « Superviseurs »", System.Windows.Forms.HorizontalAlignment.Left);
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.btnHireNewEmploye = new System.Windows.Forms.Button();
            this.employeListView = new System.Windows.Forms.ListView();
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTelephone = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMatricule = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSalaire = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Montserrat", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(315, 58);
            this.label1.TabIndex = 0;
            this.label1.Text = "Koodo Company";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.btnHireNewEmploye);
            this.groupBox1.Controls.Add(this.employeListView);
            this.groupBox1.Location = new System.Drawing.Point(22, 70);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(970, 474);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.button2.FlatAppearance.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Montserrat ExtraLight", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(264, 429);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(252, 37);
            this.button2.TabIndex = 2;
            this.button2.Text = "Calculer les salaires";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnHireNewEmploye
            // 
            this.btnHireNewEmploye.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnHireNewEmploye.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.btnHireNewEmploye.FlatAppearance.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnHireNewEmploye.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnHireNewEmploye.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHireNewEmploye.Font = new System.Drawing.Font("Montserrat ExtraLight", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHireNewEmploye.Location = new System.Drawing.Point(6, 429);
            this.btnHireNewEmploye.Name = "btnHireNewEmploye";
            this.btnHireNewEmploye.Size = new System.Drawing.Size(252, 37);
            this.btnHireNewEmploye.TabIndex = 1;
            this.btnHireNewEmploye.Text = "Engager un nouvel employé";
            this.btnHireNewEmploye.UseVisualStyleBackColor = false;
            this.btnHireNewEmploye.Click += new System.EventHandler(this.btnHireNewEmploye_Click);
            // 
            // employeListView
            // 
            this.employeListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.employeListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colName,
            this.colTelephone,
            this.colMatricule,
            this.colSalaire});
            this.employeListView.Font = new System.Drawing.Font("Montserrat", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeListView.FullRowSelect = true;
            listViewGroup1.Header = "Employés « Bureau »";
            listViewGroup1.Name = "Koodo.Employe.Bureau";
            listViewGroup2.Header = "Employés « Tâches »";
            listViewGroup2.Name = "Koodo.Employe.Tache";
            listViewGroup3.Header = "Employés « Commerciaux »";
            listViewGroup3.Name = "Koodo.Employe.Commercial";
            listViewGroup4.Header = "Employés « Superviseurs »";
            listViewGroup4.Name = "Koodo.Employe.Superviseur";
            this.employeListView.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2,
            listViewGroup3,
            listViewGroup4});
            this.employeListView.Location = new System.Drawing.Point(6, 10);
            this.employeListView.MultiSelect = false;
            this.employeListView.Name = "employeListView";
            this.employeListView.Size = new System.Drawing.Size(958, 413);
            this.employeListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.employeListView.TabIndex = 0;
            this.employeListView.UseCompatibleStateImageBehavior = false;
            this.employeListView.View = System.Windows.Forms.View.Details;
            this.employeListView.DoubleClick += new System.EventHandler(this.employeListView_DoubleClick);
            // 
            // colName
            // 
            this.colName.Text = "Nom";
            this.colName.Width = 250;
            // 
            // colTelephone
            // 
            this.colTelephone.Text = "Téléphone";
            this.colTelephone.Width = 215;
            // 
            // colMatricule
            // 
            this.colMatricule.Text = "Matricule";
            this.colMatricule.Width = 221;
            // 
            // colSalaire
            // 
            this.colSalaire.Text = "Salaire pour la période actuel";
            this.colSalaire.Width = 250;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(623, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(369, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Pour modifier les paramètres d\'un employé, effectuer un double clique dessus";
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 548);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Name = "mainForm";
            this.Text = "Gestion des salaires";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnHireNewEmploye;
        private System.Windows.Forms.ListView employeListView;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ColumnHeader colTelephone;
        private System.Windows.Forms.ColumnHeader colMatricule;
        private System.Windows.Forms.ColumnHeader colSalaire;
        private System.Windows.Forms.Label label2;
    }
}

