﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Koodo
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
        }

        private void btnHireNewEmploye_Click(object sender, EventArgs e)
        {
            Forms.EmployeForm newEmployeForm = new Forms.EmployeForm();
            if (newEmployeForm.ShowDialog() == DialogResult.OK)
            {
               reloadDatas();
            }
        }


        #region Méthodes privées

        private void reloadDatas()
        {
            employeListView.Items.Clear();

            foreach (Employe.Employe employe in Koodo.employeList)
            {
                ListViewItem newListViewItem = new ListViewItem();
                newListViewItem.Text = employe.Nom;
                newListViewItem.SubItems.Add(employe.Telephone);
                newListViewItem.SubItems.Add(employe.Matricule);
                newListViewItem.SubItems.Add("");


                newListViewItem.Group = employeListView.Groups[employe.GetType().ToString()];
                Console.WriteLine(employe.GetType().ToString());

                newListViewItem.Tag = employe;
                employeListView.Items.Add(newListViewItem);
            }
        }

        #endregion Méthodes privées

        private void employeListView_DoubleClick(object sender, EventArgs e)
        {
            Employe.Employe selectedEmploye = this.employeListView.SelectedItems[0].Tag as Employe.Employe;
            Forms.EmployeForm newEmployeForm = new Forms.EmployeForm(selectedEmploye);
            if (newEmployeForm.ShowDialog() == DialogResult.OK)
            {
                reloadDatas();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in this.employeListView.Items)
            {
                Employe.Employe employe = lvi.Tag as Employe.Employe;
                decimal salaire = employe.Salaire();
                lvi.SubItems[3].Text = salaire.ToString();
            }

            Koodo.employeList.ForEach(x => x.Salaire(true));
        }
    }
}
