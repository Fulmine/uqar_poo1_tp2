﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Koodo.Employe;

namespace Koodo.Forms
{
    public partial class EmployeForm : Form
    {
        #region Déclarations

        private NumericUpDown numExtra1 = new NumericUpDown();
        private Label lblExtra1 = new Label();

        private NumericUpDown numExtra2 = new NumericUpDown();
        private Label lblExtra2 = new Label();

        private Employe.Employe currentEmploye = null;

        #endregion Déclarations

        public EmployeForm()
        {
            InitializeComponent();
            appyStyle();
            this.Text = "Ajout d'un nouvel employé";
        }

        public EmployeForm(Employe.Employe employe)
        {
            InitializeComponent();
            appyStyle();

            currentEmploye = employe;
            this.Text = String.Format("Modification de l'employé « {0} »", currentEmploye.Nom);
            lblEmployeName.Text = employe.Nom;
            txtName.Text = employe.Nom;
            txtTelephone.Text = employe.Telephone;
            txtMatricule.Text = employe.Matricule;

            txtName.Enabled = false;
            txtTelephone.Enabled = false;
            txtMatricule.Enabled = false;

            switch (currentEmploye.GetType().ToString())
            {
                case "Koodo.Employe.Bureau":
                    this.cbFonction.SelectedIndex = 0;
                    break;
                case "Koodo.Employe.Tache":
                    this.cbFonction.SelectedIndex = 1;
                    break;
                case "Koodo.Employe.Commercial":
                    this.cbFonction.SelectedIndex = 2;
                    break;
                case "Koodo.Employe.Superviseur":
                    this.cbFonction.SelectedIndex = 3;
                    break;
            }
            this.cbFonction.Enabled = false;
        }

        #region Méthodes privées

        private void appyStyle()
        {
            #region Extra 1

            this.numExtra1.Font = new System.Drawing.Font("Montserrat", 11.25F);
            this.numExtra1.Location = new System.Drawing.Point(3, 28);
            this.numExtra1.Maximum = new decimal(new int[] { 10000000, 0, 0, 0});
            this.numExtra1.Name = "numExtra1";
            this.numExtra1.Size = new System.Drawing.Size(536, 26);

            this.lblExtra1.AutoSize = true;
            this.lblExtra1.Margin = new System.Windows.Forms.Padding(0);
            this.lblExtra1.Font = new System.Drawing.Font("Montserrat", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExtra1.Name = "lblSalaireFixe";

            #endregion Extra 1

            #region Extra 2

            this.numExtra2.Font = new System.Drawing.Font("Montserrat", 11.25F);
            this.numExtra2.Location = new System.Drawing.Point(3, 28);
            this.numExtra2.Maximum = new decimal(new int[] { 10000000, 0, 0, 0 });
            this.numExtra2.Name = "numExtra2";
            this.numExtra2.Size = new System.Drawing.Size(536, 26);

            this.lblExtra2.AutoSize = true;
            this.lblExtra2.Margin = new System.Windows.Forms.Padding(0);
            this.lblExtra2.Font = new System.Drawing.Font("Montserrat", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExtra2.Name = "lblExtra2";

            #endregion Extra 2

        }

        #endregion Méthodes privées


        #region Bouton: Sauvegarde
        private void btnSave_Click(object sender, EventArgs e)
        {

            if (!String.IsNullOrWhiteSpace(txtName.Text)
                && !String.IsNullOrWhiteSpace(txtTelephone.Text)
                && !String.IsNullOrWhiteSpace(txtMatricule.Text))
            {

                if (currentEmploye != null)
                {
                    DialogResult = DialogResult.OK;

                    switch (cbFonction.Text)
                    {
                        case "Tâche":
                            ((Employe.Tache) currentEmploye).nbrHeures = this.numExtra2.Value;
                            break;

                        case "Commercial":
                            ((Employe.Commercial)currentEmploye).montantVente = this.numExtra2.Value;
                            break;

                        case "Superviseur":
                            ((Employe.Superviseur)currentEmploye).objectif = this.numExtra2.Value;
                            break;
                    }

                    DialogResult = DialogResult.OK;
                    return;
                }


                Employe.Employe newEmploye = null;

                switch (cbFonction.Text)
                {
                    case "Bureau":
                        newEmploye = new Bureau(this.txtName.Text, this.txtTelephone.Text, this.txtMatricule.Text, this.numExtra1.Value);
                        break;

                    case "Tâche":
                        newEmploye = new Tache(this.txtName.Text, this.txtTelephone.Text, this.txtMatricule.Text, this.numExtra1.Value);
                        if (this.numExtra2.Value > 0)
                            ((Employe.Tache)newEmploye).nbrHeures = this.numExtra2.Value;
                        break;

                    case "Commercial":
                        newEmploye = new Commercial(this.txtName.Text, this.txtTelephone.Text, this.txtMatricule.Text, this.numExtra1.Value);
                        if (this.numExtra2.Value > 0)
                            ((Employe.Commercial)newEmploye).montantVente = this.numExtra2.Value;
                        break;

                    case "Superviseur":
                        newEmploye = new Superviseur(this.txtName.Text, this.txtTelephone.Text, this.txtMatricule.Text, this.numExtra1.Value);
                        if (this.numExtra2.Value > 0)
                            ((Employe.Superviseur)newEmploye).objectif = this.numExtra2.Value;
                        break;
                }

                if (newEmploye != null)
                {
                    Koodo.employeList.Add(newEmploye);
                    Console.WriteLine(newEmploye.ToString());
                    DialogResult = DialogResult.OK;
                    return;
                }
            }

            DialogResult = DialogResult.Abort;
        }

        #endregion Bouton: Sauvegarde

        private void cbFonction_SelectedIndexChanged(object sender, EventArgs e)
        {
            flpExtra.Controls.Clear();
            switch (cbFonction.Text)
            {
                case "Bureau":

                    this.numExtra1.Value = 0;
                    this.lblExtra1.Text = "Salaire fixe";

                    if (currentEmploye != null)
                    {
                        this.numExtra1.Value = ((Employe.Bureau)currentEmploye).SalaireFixe;
                        this.numExtra1.Enabled = false;
                    }

                    flpExtra.Controls.Add(this.lblExtra1);
                    flpExtra.Controls.Add(this.numExtra1);
                    break;

                case "Tâche":
                    this.numExtra1.Value = 0;
                    this.lblExtra1.Text = "Taux horaire";

                    flpExtra.Controls.Add(this.lblExtra1);
                    flpExtra.Controls.Add(this.numExtra1);

                    this.numExtra2.Value = 0;
                    this.lblExtra2.Text = "Nombre d'heures";

                    if (currentEmploye != null)
                    {
                        this.numExtra1.Value = ((Employe.Tache)currentEmploye).TauxHoraire;
                        this.numExtra1.Enabled = false;
                        this.numExtra2.Value = ((Employe.Tache)currentEmploye).nbrHeures;
                    }

                    flpExtra.Controls.Add(this.lblExtra2);
                    flpExtra.Controls.Add(this.numExtra2);
                    break;

                case "Commercial":

                    this.numExtra1.Value = 0;
                    this.lblExtra1.Text = "Salaire fixe";

                    flpExtra.Controls.Add(this.lblExtra1);
                    flpExtra.Controls.Add(this.numExtra1);

                    this.numExtra2.Value = 0;
                    this.lblExtra2.Text = "Montant total des ventes";

                    if (currentEmploye != null)
                    {
                        this.numExtra1.Value = ((Employe.Commercial)currentEmploye).SalaireFixe;
                        this.numExtra1.Enabled = false;
                        this.numExtra2.Value = ((Employe.Commercial)currentEmploye).montantVente;
                    }


                    flpExtra.Controls.Add(this.lblExtra2);
                    flpExtra.Controls.Add(this.numExtra2);
                    break;

                case "Superviseur":
                    this.numExtra1.Value = 0;
                    this.lblExtra1.Text = "Salaire fixe";

                    flpExtra.Controls.Add(this.lblExtra1);
                    flpExtra.Controls.Add(this.numExtra1);

                    this.numExtra2.Value = 0;
                    this.lblExtra2.Text = "Objetif de vente";

                    if (currentEmploye != null)
                    {
                        this.numExtra1.Value = ((Employe.Superviseur)currentEmploye).SalaireFixe;
                        this.numExtra1.Enabled = false;
                        this.numExtra2.Value = ((Employe.Superviseur)currentEmploye).objectif;
                    }

                    flpExtra.Controls.Add(this.lblExtra2);
                    flpExtra.Controls.Add(this.numExtra2);
                    break;
            }
        }

        private void EmployeForm_Load(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
